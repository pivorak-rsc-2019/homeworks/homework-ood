require 'rails_helper'

RSpec.describe QuestService, tdd: true do
  it 'create quest' do
    quest = QuestService.call(:create, params)

    expect(Quest.find(quest.id)).to be_an_instance_of(Quest)
  end

  it 'update quest' do
    quest = QuestService.call(:update, params)

    expect(quest).to be_an_instance_of(Quest)
  end

  it 'delete quest' do
    quest = QuestService.call(:delete, params)

    expect(quest).to be_an_instance_of(Quest)
  end
end
