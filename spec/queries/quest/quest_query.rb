# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QuestQuery, tdd: true do
  let(:user) { create(:user) }
  let(:condition) { { id: user.id } }
  before do
    create(:quest, user_id: user.id )
    create(:quest, user_id: Faker::Number.number(10) )
  end

  describe '#where' do
    subject(:where) { |condition| described_class.new.where(condition) }

    it 'returns filtered quests' do
      quests = where(condition)

      expect(quests.size).to eq(1)
      expect(quests.first.user_id).to eq(user.id)
    end
  end

  describe '#all' do
    subject(:all) { described_class.new.all }

    it 'returns all quests' do
      expect(all.size).to eq(2)
    end
  end
end
