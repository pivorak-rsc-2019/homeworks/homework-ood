FactoryBot.define do
  factory :user, aliases: [:author] do
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    role { 'user' }
  end
end
