FactoryBot.define do
  factory :quest do
    name { Faker::TvShows::SiliconValley.invention }
    description { Faker::Movies::Lebowski.quote }
    user_id { Faker::IDNumber.number(10) }
  end
end
