# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QuestPolicy, tdd: true do
  let(:quest) { create(:quest) }
  subject { described_class.new(current_user: user, resource: quest) }

  it 'have moderate policy' do
    expect(described_class).to respond_to(:able_to_moderate?)
  end

  context 'when current user regular user' do
    let(:user) { create(:user) }

    it { expect(subject).not_to be_able_to_moderate }
  end

  context 'when current user is an admin' do
    let(:user) { create(:user, role: :admin) }

    it { expect(subject).to be_able_to_moderate }
  end

  context 'when current user is a quest moderator' do
    let(:user) { create(:user).tap { |user| user.quests << quest } }

    it { expect(subject).to be_able_to_moderate }
  end
end
