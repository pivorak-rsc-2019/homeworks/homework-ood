require 'rails_helper'

RSpec.describe CurrentLocation, type: :model, tdd: true do
  let!(:current_location) { FactoryBot.create(:current_location) }
  let!(:attributes) { %i[lat lon] }

  it 'has attributes' do
    expect(current_location).to respond_to(attributes)
  end

  it 'has a valid factory' do
    expect(current_location).to be_valid
  end

  context 'validations' do
    it { should validate_presence_of(:lat) }
    it { should validate_presence_of(:lon) }
  end

  context 'relations' do
    it { is_expected.to belong_to(:user) }
  end
end
